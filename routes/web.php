<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LayoutController@home');
Route::get('/anggota/list', 'AjaxController@index');
Route::get('/anggota/add', 'AjaxController@index');
Route::post('/anggota/store', 'AjaxController@store');
Route::delete('/anggota/delete/{id}', 'AjaxController@delete');
Route::get('/anggota/{id}', 'AjaxController@show');
Route::post('/anggota/edit/{id}', 'AjaxController@update');
