<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnggotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota', function (Blueprint $table) {
            $table->bigInteger('anggota_nik', false, true)->length(16);
            $table->char('anggota_fullname');
            $table->integer('anggota_gender')->default(1);
            $table->date('anggota_birthday');
            $table->char('anggota_photo')->default('avatar5.png');
            $table->char('anggota_address');
            $table->integer('anggota_role')->default(0);
            $table->timestamps();
            $table->primary('anggota_nik');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota');
    }
}
