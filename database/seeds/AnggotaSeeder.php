<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($x = 1; $x <= 10; $x++){
            DB::table('anggota')->insert([
                'anggota_nik' => $faker->numberBetween(3000000000000000,3500000000000000),
                'anggota_fullname' => $faker->name,
                'anggota_gender' => $faker->numberBetween(1,2),
                'anggota_birthday'=> $faker->dateTime($max = 'now'),
                'anggota_address' => $faker->address,
            ]);
        }
    }
}
