@extends('layout')
@section('html_head')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection
@section('content_main')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Daftar Keanggotaan</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-toggle="modal" data-target="#anggota-add">
                        <i class="fas fa-plus"> Tambah anggota baru</i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body ">
                <table id="example1" class="table table-bordered table-striped table-responsive-md">
                    <thead>
                        <tr>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Usia</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="anggota_tbody">
                        @foreach ($anggota as $a)
                        <tr id="anggota_row_id_{{$a->anggota_nik}}">
                            <td>{{ $a->anggota_nik }}</td>
                            <td>{{ $a->anggota_fullname }}</td>
                            <td>{{ $a->anggota_gender == 1 ? "Laki-Laki" : "Perempuan" }}</td>
                            <?php
                                $ticketTime = strtotime( $a->anggota_birthday);

                                // This difference is in seconds.
                                $difference =  (time() - $ticketTime)/3600/24/30/12;
                                ?>
                            <td>{{ $a->anggota_birthday }}</td>
                            <td>{{ floor($difference) }} Tahun</td>
                            <td>{{ $a->anggota_address }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" onclick="editAnggota({{ $a->anggota_nik }})"
                                        class="btn btn-warning"><i class="far fa-edit"></i></button>
                                    <button type="button" onclick="delAnggota({{ $a->anggota_nik }})"
                                        class="btn btn-danger" data-id="{{ $a->anggota_nik }}"><i
                                            class="fas fa-trash-alt"></i></button>
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Usia</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<div class="modal fade" id="anggota-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Primary Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img src="{{asset('dist/img/avatar5.png')}}" alt="Foto Anggota" class="img-thumbnail">
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="InputFile" onchange="getPicture(this)">
                            <label class="custom-file-label" for="exampleInputFile" id="PictureName">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Upload</span>
                        </div>
                    </div>
                    <br />
                </div>
                <div class="card">
                    <div class="card-body ">
                        <form class="form-horizontal" id="addAnggota">
                            <div class="form-group">
                                <label for="inputNIK" class="col-sm-2 control-label">NIK</label>
                                <div class="col-sm-10">
                                    <input type="number" name="nik" class="form-control" id="inputNIK"
                                        placeholder="Nomor KTP">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Nama</label>

                                <div class="col-sm-10">
                                    <input type="text" name="nama" class="form-control" id="inputName"
                                        placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputGender" class="col-sm-2 control-label">Jenis Kelamin</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="jenkel" id="inputGender">
                                        <option value="1">Laki - Laki</option>
                                        <option value="2">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputBirthday" class="col-sm-2 control-label">Tanggal Lahir</label>

                                <div class="col-sm-10">
                                    <input type="date" name="tglLahir" class="form-control" id="inputBirthday">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress" name="alamat" class="col-sm-2 control-label">Alamat</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputAddress" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button id="btn-add" class="btn btn-success">Tambah</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between"></div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="anggota-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-default">
            <div class="modal-header">
                <h4 class="modal-title">Primary Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img src="{{asset('dist/img/avatar5.png')}}" alt="Foto Anggota" class="img-thumbnail">
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="InputFile" onchange="getPicture(this)">
                            <label class="custom-file-label" for="exampleInputFile" id="PictureName">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Upload</span>
                        </div>
                    </div>
                    <br />
                </div>
                <div class="card">
                    <div class="card-body ">
                        <form class="form-horizontal" id="editAnggota">
                            <div class="form-group">
                                <label for="inputNIK" class="col-sm-2 control-label">NIK</label>
                                <div class="col-sm-10">
                                    <input type="number" name="nik" class="form-control" id="inputNIK2"
                                        placeholder="Nomor KTP">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Nama</label>

                                <div class="col-sm-10">
                                    <input type="text" name="nama" class="form-control" id="inputName2"
                                        placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputGender" class="col-sm-2 control-label">Jenis Kelamin</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="jenkel" id="inputGender2">
                                        <option value="1">Laki - Laki</option>
                                        <option value="2">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputBirthday" class="col-sm-2 control-label">Tanggal Lahir</label>

                                <div class="col-sm-10">
                                    <input type="date" name="tglLahir" class="form-control" id="inputBirthday2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress" name="alamat" class="col-sm-2 control-label">Alamat</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputAddress2" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button id="btn-save" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between"></div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@section('html_script')
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- MomentJS -->
<script src="{{asset('dist/js/pages/moment.min.js')}}"></script>
<script>
    function editAnggota(id) {
        var user_id = $(this).data('id');
        $.get('/anggota' + '/' + id, function (anggota) {

            $('#anggota-edit').modal('show');
            $('#inputNIK2').val(anggota.anggota_nik);
            $('#inputName2').val(anggota.anggota_fullname);
            $('#inputGender2').val(anggota.anggota_gender);
            $('#inputBirthday2').val(anggota.anggota_birthday);
            $('#inputAddress2').val(anggota.anggota_address)
        })
    }

    function delAnggota(id) {
        confirm("Are You sure want to delete !");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            type: "DELETE",
            url: "/anggota/delete" + '/' + id,
            success: function (anggota) {
                $("#anggota_row_id_" + id).remove();
            },
            error: function (anggota) {
                console.log('Error:', anggota);
            }
        });
    }

    function getPicture(data) {
        var picturename = data.value;
        document.getElementById('PictureName').innerHTML = picturename
    }
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });
    jQuery(document).ready(function () {
        jQuery('#btn-add').click(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "{{ url('/anggota/store') }}",
                method: 'POST',
                data: {
                    nik: jQuery('#inputNIK').val(),
                    nama: jQuery('#inputName').val(),
                    jenKel: jQuery('#inputGender').val(),
                    tglLahir: jQuery('#inputBirthday').val(),
                    alamat: jQuery('#inputAddress').val(),
                },
                success: function (result) {
                    var years = moment().diff(result.anggota_birthday, 'years');
                    var gender = (result.anggota_gender == 1) ? "Laki-Laki" : "Perempuan";
                    var anggota_res = '<tr><td>' + result.anggota_nik + '</td> <td>' +
                        result.anggota_fullname + '</td><td>' + gender + ' </td><td>' +
                        result.anggota_birthday + '</td><td> '+ years +' Tahun </td><td>' + result.anggota_address +
                        '</td>';
                    anggota_res +=
                        '<td><div class="btn-group"><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#anggota-edit"><i class="far fa-edit"></i></button>';
                    anggota_res +=
                        '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#anggota-delete"><i class="fas fa-trash-alt"></i></button> </div> </td>';
                    $("#anggota_tbody").prepend(anggota_res);
                    $('#addAnggota').trigger("reset");
                    $('#anggota-add .close').click();
                },
                error: function (result) {
                    console.log(result);
                }
            });
        })

        jQuery('#btn-save').click(function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "/anggota/edit/ " + $('#inputNIK2').val(),
                method: 'POST',
                data: {
                    nik: jQuery('#inputNIK2').val(),
                    nama: jQuery('#inputName2').val(),
                    jenKel: jQuery('#inputGender2').val(),
                    tglLahir: jQuery('#inputBirthday2').val(),
                    alamat: jQuery('#inputAddress2').val(),
                },
                success: function (anggota) {
                    var years = moment().diff(jQuery('#inputBirthday2').val(), 'years');
                    var gender = jQuery('#inputGender2').val() ? "Laki-Laki" : "Perempuan";
                    var anggota_res = '<tr id="anggota_row_id_' + jQuery("#inputNIK2").val() + '"><td>' + jQuery("#inputNIK2").val() + '</td> <td>' +
                        jQuery("#inputName2").val() + '</td><td>' + gender + ' </td><td>' +
                            jQuery("#inputBirthday2").val() + '</td><td> '+ years +' Tahun </td><td>' + jQuery("#inputAddress2").val() +
                        '</td>';
                    anggota_res +=
                        '<td><div class="btn-group"><button type="button" onclick="editAnggota('+ jQuery("#inputNIK2").val() + ')" class="btn btn-warning"><i class="far fa-edit"></i></button>';
                    anggota_res +=
                        '<button type="button" onclick="delAnggota(' + jQuery("#inputNIK2").val() + ')" class="btn btn-danger" data-id="' + jQuery("#inputNIK2").val() + '"><i class="fas fa-trash-alt"></i></button> </div> </td>';
                    $("#anggota_row_id_" + jQuery('#inputNIK2').val()).replaceWith(anggota_res);
                    $('#editAnggota').trigger("reset");
                    $('#anggota-edit .close').click();
                },
                error: function (result) {
                    console.log(result);
                }
            });
        });
    })
</script>
@endsection
@endsection
