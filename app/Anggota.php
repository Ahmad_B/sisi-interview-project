<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
   protected $table = "anggota";
   protected $fillable = ['anggota_nik','anggota_fullname','anggota_gender','anggota_birthday','anggota_address'];
}
