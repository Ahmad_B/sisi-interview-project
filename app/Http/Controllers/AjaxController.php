<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anggota;

class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = Anggota::all();
        return view('listAnggota',['anggota' => $anggota ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "wkwkwkwkwkw";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,[
    	// 	'nama' => 'required',
    	// 	'alamat' => 'required'
    	// ]);

        $response = Anggota::create([
    		'anggota_nik' => $request->nik,
            'anggota_fullname' => $request->nama,
            'anggota_gender' => $request->jenKel,
            'anggota_birthday' => $request->tglLahir,
            'anggota_address' => $request->alamat,
        ]);
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $column_name = "anggota_nik";
        $anggota = Anggota::where($column_name,$id)->first();
        return $anggota;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $column_name = "anggota_nik";
        $anggota = Anggota::where($column_name,$id);
        return ['anggota' => $anggota];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $column_name = "anggota_nik";
        $anggota = Anggota::where($column_name,$id)->update([
    		'anggota_nik' => $request->nik,
            'anggota_fullname' => $request->nama,
            'anggota_gender' => $request->jenKel,
            'anggota_birthday' => $request->tglLahir,
            'anggota_address' => $request->alamat,
        ]
        );

        return $anggota;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $column_name = "anggota_nik";
        $anggota = Anggota::where($column_name,$id)->delete();
        return $anggota;
    }
}
