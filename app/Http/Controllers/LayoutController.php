<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LayoutController extends Controller
{
    public function home(){
        return view('home');
    }
    public function listAnggota(){
        return view('listAnggota');
    }
}
