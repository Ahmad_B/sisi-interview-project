-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2019 at 09:33 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisi_koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `anggota_nik` bigint(20) UNSIGNED NOT NULL,
  `anggota_fullname` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anggota_gender` int(11) NOT NULL DEFAULT '1',
  `anggota_birthday` date NOT NULL,
  `anggota_photo` char(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'avatar5.png',
  `anggota_address` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anggota_role` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`anggota_nik`, `anggota_fullname`, `anggota_gender`, `anggota_birthday`, `anggota_photo`, `anggota_address`, `anggota_role`, `created_at`, `updated_at`) VALUES
(3275434742198264, 'Amelia Winarsih', 2, '1990-05-30', 'avatar5.png', 'Gg. Bakhita No. 581, Bau-Bau 43365, BaBel', 0, NULL, '2019-08-22 00:30:18'),
(3315592397375653, 'Maryadi Permadi', 1, '1990-03-07', 'avatar5.png', 'Jr. Imam No. 685, Yogyakarta 11834, SumSel', 0, NULL, '2019-08-22 00:29:51'),
(3335185246068447, 'Ghaliyati Rahmawati', 1, '2002-03-21', 'avatar5.png', 'Gg. Laswi No. 459, Pasuruan 40638, SumSel', 0, NULL, NULL),
(3476042528262348, 'Gandi Galang Situmorang S.Kom', 2, '1972-03-24', 'avatar5.png', 'Jr. Jayawijaya No. 56, Ternate 53617, Aceh', 0, NULL, NULL),
(112233445566778899, 'wakijo trlalala', 1, '2015-06-19', 'avatar5.png', 'asdfasdfasdf', 0, '2019-08-22 00:32:14', '2019-08-22 00:32:14');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_08_21_212328_create_anggotas_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`anggota_nik`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
